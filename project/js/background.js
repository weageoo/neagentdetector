var extension = opera.extension
var tabs = extension.tabs
var toolbar = opera.contexts.toolbar
var preferences = widget.preferences
var storage = window.localStorage

function log(text) {
    extension.broadcastMessage({
        type:'log',
        text:text
    })
}

// settings

Setting = function(key, readonly) {
    if (typeof readonly !== 'undefined' && !readonly) {
        this.key = key // save key if readwrite setting
    }
    this.value = eval(preferences[key])

    return this
}
Setting.prototype.update = function(value) {
    if (typeof this.key === 'undefined') {
        return
    }

    this.value = value
    preferences[this.key] = value.toString()

    extension.broadcastMessage({
        type:'settings-update',
        settings:JSON.stringify(Settings)
    })
}

var Settings = {
    General: {
        CheckPnoneNumberURL: new Setting('General.CheckPnoneNumberURL'),
        IsPhoneNumberDetectionEnabled: new Setting('General.IsPhoneNumberDetectionEnabled', false),
        DelayBeforePhoneNumberDetectionMsec: new Setting('General.DelayBeforePhoneNumberDetectionMsec')
    },
    Neagent: {
        Bar: {
            SourceURL: new Setting('Neagent.Bar.SourceURL'),
            HeightPx: new Setting('Neagent.Bar.HeightPx'),
            Enabled: new Setting('Neagent.Bar.Enabled', false)
        }
    },
    Popup: {
        ClosePopupAfterMsec: new Setting('Popup.ClosePopupAfterMsec', false)
    },
    Rules: {
        ElentHandling: {
            Disallow: {
                DOMNodeInserted: new Setting('Rules.ElentHandling.Disallow.DOMNodeInserted')
            }
        },
        Timing: {
            DelayPhoneNumberDetection: new Setting('Rules.Timing.DelayPhoneNumberDetection')
        }
    }
}

// checked phone numbers cache

var Cache = {

    base: {},

    get: function(phoneNumber) {
        if (this.check(phoneNumber)) {
            return this.base[phoneNumber]
        }
    },

    set: function(phoneNumber, value) {
        if (this.check(phoneNumber)) {
            if (this.base[phoneNumber] === value) {
                return
            }
        }
        this.base[phoneNumber] = value

        var cacheJSON = JSON.stringify(this.base)

        preferences['PHONE_CACHE'] = cacheJSON

        extension.broadcastMessage({
            type:'cache-update',
            cache:cacheJSON
        })
    },

    check: function(phoneNumber) {
        return (phoneNumber in this.base)
    }
}

// browser toolbar upgrade

var ToolbarButton
jHelpers.contentLoaded(window, function(){
    var ToolbarUIItemProperties = {
        title: 'Neagent Detector ' + widget.version,
        icon: 'icons/toolbar-icon-18x18.png',
        popup: {href: 'popup.html', width: 300, height: 210}
    }
    ToolbarButton = toolbar.createItem(ToolbarUIItemProperties)
    toolbar.addItem(ToolbarButton)
})

// messaging

jHelpers.contentLoaded(window, function() {
    var _checkedPhones = storage['PHONE_CACHE'] | preferences['PHONE_CACHE']
    if (_checkedPhones) {
        Cache.base = JSON.parse(_checkedPhones)
    }

    opera.extension.onconnect = function(event) {
        event.source.postMessage({
            type:'init',
            settings:JSON.stringify(Settings),
            cache:JSON.stringify(Cache.base)
        })
    }
    opera.extension.onmessage = function(event) {
        var message = event.data

        switch (message.type) {
            case 'check-phone-number':
                checkPhoneNumber(message.phoneNumber, function(answer) {
                    event.source.postMessage({
                        type:'check-phone-number-ok',
                        phoneNumber:message.phoneNumber,
                        response:answer
                    })
                })
                break
        }
    }
})

// functions

var normalizePhoneNumber = function (phone) {
    var result = ''

    for (var i = 0; i < phone.length; ++i) {
    	var ch = phone[i]
        if (ch >= '0' && ch <= '9') {
        	result += ch
        }
    }

    result = result.replace(/^(:?375)/, '80') // to BY internal format

    return result
}

var checkPhoneNumber = function (normalizedPhone, onCheck, onError) {
    try {
    	var xhr = new XMLHttpRequest()

        xhr.onreadystatechange = function() {
        	if (this.readyState == 4) {
        		if (this.status == 200) {
                    Cache.set(normalizedPhone, this.responseText)
            		if (onCheck) {
                		onCheck(this.responseText)
              		}
            	}
            	else {  // error
              		if (onError) {
                		onError(this.responseText)
            		}
            	}
        	}
        }

        var params = 'phonenumber=' + encodeURIComponent(normalizedPhone)

        xhr.open('POST', Settings.General.CheckPnoneNumberURL.value, true)
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')
        xhr.setRequestHeader('Content-length', params.length)
        xhr.setRequestHeader('Connection', 'close')
        xhr.send(params)
    } 
    catch (err) {
    	opera.postError(err)
    }
}