// ==UserScript==
// @project Neagent Detector
// @include *
// @match *
// ==/UserScript==

(function () {

	// Variables

	var defaultStyle = 'text-decoration:underline;color:gray;cursor:pointer'

	var patterns = {
		humanPhoneNumber: /(?:(?:\+?[ ]?375)[ -]?[(-]?\d\d[)-]?|8?[ -]?[(-]?0\d\d[)-]?)[ -]?\d[ -]?\d[ -]?\d[ -]?\d[ -]?\d[ -]?\d[ -]?\d/gi,
		normalizedPhoneNumber: /^(?:80)\d{9}$/,
		byRegionPhoneNumber: /^(?:\+375|375|80)(?:(?:1(?:5(?:1[1-5]|2\d|6[1-4]|9[1-7])|6(?:[235]\d|4[1-7])|7\d{2})|2(?:1(?:[246]\d|3[0-35-9]|5[1-9])|2(?:[235]\d|4[0-8])|3(?:2\d|3[02-79]|4[024-7]|5[0-7])))\d{5}|(?:2(?:5[5679]|9[1-9])|33\d|44\d)\d{6})$/
	}

	var wrappedElementPrefix = 'NeagentDetector_Wrapped', 
	    wrappedElementFormatString = wrappedElementPrefix + '_{0}',
	    injectNeagentBarClass = 'NeagentDetector_InjectedBar',
	    notifierClass = 'NeagentDetector_Notifier'

	var Notifier, Cache = {}, Settings = {}, Port

	var postMessage = function(msg) {
		Port && Port.postMessage(msg)
	}

	// Prototypes

	String.prototype.format = function() {
		var args = arguments
		return this.replace(/{(\d+)}/g, function(match, number) { 
			return typeof args[number] != 'undefined' ? args[number] : match
		})
	}

	Array.prototype.contains = function(obj) {
	    var i = this.length;
	    while (i--) {
	        if (this[i] === obj) {
	            return true;
	        }
	    }
    	return false;
	}

	// Helpers

	var Helpers = {

		contentLoaded: function (fn) {
			var done = false, top = true, win = window,
			doc = win.document, root = doc.documentElement,

			add = doc.addEventListener ? 'addEventListener' : 'attachEvent',
			rem = doc.addEventListener ? 'removeEventListener' : 'detachEvent',
			pre = doc.addEventListener ? '' : 'on',

			init = function(e) {
				if (e.type == 'readystatechange' && doc.readyState != 'complete') return;
				(e.type == 'load' ? win : doc)[rem](pre + e.type, init, false);
				if (!done && (done = true)) fn.call(win, e.type || e);
			},

			poll = function() {
				try { root.doScroll('left'); } catch(e) { setTimeout(poll, 50); return; }
				init('poll');
			};

			if (doc.readyState == 'complete') fn.call(win, 'lazy');
			else {
				if (doc.createEventObject && root.doScroll) {
					try { top = !win.frameElement; } catch(e) { }
					if (top) poll();
				}
				doc[add](pre + 'DOMContentLoaded', init, false);
				doc[add](pre + 'readystatechange', init, false);
				win[add](pre + 'load', init, false);
			}
		},

		injectNeagentBar: function (remove) {
			if (window.location != window.parent.location) {
				return
			}

			if (typeof remove !== 'undefined' && remove) {
				var bar = document.getElementsByClassName(injectNeagentBarClass)[0]
				document.body.removeChild(bar)
				document.body.style.top = 0

				return
			}

			var barScr = Settings.Neagent.Bar.SourceURL.value
			var barHeightPx = Settings.Neagent.Bar.HeightPx.value

			var bar = document.createElement('div')
			bar.className = injectNeagentBarClass
			bar.style = 'margin:0px;padding:0px;overflow:hidden;top:0px;right:0;left:0;position:fixed;z-index:2147483647;height:' + barHeightPx + 'px'

			var iframe = document.createElement('iframe')
			iframe.src = barScr
			iframe.style = 'position:fixed;overflow:hidden;z-index:2147483647;overflow-x:auto;overflow-y:auto;width:100%;margin-top:0px;height:' + barHeightPx + 'px'
			iframe.frameBorder = '0'
			iframe.scrolling = 'no'

			bar.appendChild(iframe)
			if (document.body.children[0]) {
				document.body.insertBefore(bar, document.body.children[0])
			} else {
				document.body.appendChild(bar)
			}

			// add padding
			document.body.style = 'top:' + barHeightPx + 'px;position:relative;margin-right:0px;margin-left:0px;margin-top:0px'
		},

		phoneNumberToInternationalFormat: function (normalizedPhone) {
			var p = normalizedPhone
			return '+375 ({0}{1}) {2}{3}{4}-{5}{6}-{7}{8}'
				.format(p[2], p[3], p[4], p[5], p[6], p[7], p[8], p[9], p[10])
		},

		normalizePhoneNumber: function(phone) {
			var result = ''

			for (var i = 0; i < phone.length; ++i) {
				var ch = phone[i]
				if (ch >= '0' && ch <= '9') {
					result += ch
				}
			}

			result = result.replace(/^(:?375)/, '80') // to BY internal format
			result = result.replace(/^0/, '80') // add lost 8

			//if (patterns.normalizedPhoneNumber.test(result)) {
			//	return result
			//}

			return result
		},

		notificationCenter : function (options) {
			if (typeof options == 'undefined') options = new Object()

			var element = document.createElement('ul')
			element.className = notifierClass
			element.style.position = 'fixed'
			element.style.top = (Settings.Neagent.Bar.Enabled.value) ? (Settings.Neagent.Bar.HeightPx.value + 15) + 'px': '15px'
			element.style.right = '15px'
			element.style.zIndex = '2147483647'
			element.style.color = 'black'

			document.body.appendChild(element)

			this.notify = function (options) {
				var n = new notification(options)

				if (!this.element().firstChild) {
					this.element().appendChild(n.element())
				} else {
					this.element().insertBefore(n.element() , this.element().firstChild)
				}

				if (options.fadein == true) {
					n.element().style.opacity = 0
					function fadeIn (el) {
						if (el.style.opacity >= 1) return

						el.style.opacity = parseFloat(el.style.opacity) + 0.02
						var timeout = window.setTimeout(fadeIn, 10, el)
					}

					fadeIn (n.element())
				}

				return n
			}

			this.element = function () {
				return element
			}

			this.destroy = function () {
			}

			function notification (options) {
				if (typeof options == 'undefined') options = new Object ()

				var element = document.createElement ('li'),
			        title = document.createElement('div'),
			        content = document.createElement('div'),
			        image = document.createElement('img'),
			        closeButton = document.createElement('span')

				element.style.listStyle = 'none'
				element.style.fontSize = '14px'
				element.style.background = '#fafafa'
				element.style.padding = '5px'
				element.style.margin = '5px 0'

				var border = '3px solid #2f73b2'

				if (typeof options.error != 'undefined' && options.error == true) {
					border = '3px solid #b80000'
				}
				element.style.border = border
				element.style.borderRadius = '5px'
				element.style.height = '40px'
				element.style.width = '200px'
				element.style.position = 'relative'

				if (typeof options.title != 'undefined') {
					var title = document.createElement('div')
					title.innerHTML = options.title
					title.style.font = 'normal bold 14px/18px Arial'
					element.appendChild(title)
				}

				if (typeof options.content != 'undefined') {
					content.innerHTML = options.content
					content.style.font = 'normal bold 14px/18px Arial'
				}

				element.appendChild(content)
				
				this.show = function () {
					element.style.display = 'block'
				}

				this.hide = function () {
					element.style.display = 'none'
				}
				
				destroy = function (element) {
					element.parentNode.removeChild(element)
				}

				this.destroy = function () {
					destroy(this.element())
				}

				this.title = function (text) {
					if (typeof text == 'undefined') {
						return title.textContent
					}
					title.textContent = text
				}

				this.text = function (text) {
					if (typeof text == 'undefined') {
						return content.textContent
					}
					content.textContent = text
				}

				this.html = function (html) {
					if (typeof html == 'undefined') {
						return content.innerHTML
					}
					content.innerHTML = html
				}

				this.element = function () {
					return element
				}

				function fadeOut (el) {
					if (el.style.opacity <= 0){
						destroy(el)
						return
					}

					el.style.opacity = parseFloat(el.style.opacity) - 0.1

					if (el.style.opacity < 0.2) {
						el.style.opacity = 0
					}

					var timeout = window.setTimeout(fadeOut, 10, el)
				}
							
				if (typeof options.autoClose != 'undefined') {
					var time = parseInt(options.autoClose)
					if (time > 0) {
						window.setTimeout(function (element) {
							element.style.opacity = 1
							fadeOut(element);
						} , time, element)
					}
				}

				return this;
			}
		}
	}

	// Main

	Helpers.contentLoaded(function () {

		opera.extension.onmessage = function(event){
			var message = event.data

			switch (message.type) {
				case 'init':
					Settings = JSON.parse(message.settings) // first load settings!
					Port = event.source
					Notifier = new Helpers.notificationCenter()
					Cache = JSON.parse(message.cache)

					if (Settings.General.IsPhoneNumberDetectionEnabled.value && Settings.Neagent.Bar.Enabled.value) {
						Helpers.injectNeagentBar()
					}

					if (Settings.Rules.Timing.DelayPhoneNumberDetection.value.contains(document.domain)) {
						window.setInterval(runDetection, Settings.General.DelayBeforePhoneNumberDetectionMsec) // delay for spesific sites
					} else {
						runDetection()
					}
					break
				case 'cache-update':
					Cache = JSON.parse(message.cache)
					break
				case 'settings-update':
					var oldSettings = Settings
					var newSettings = JSON.parse(message.settings)

					if (oldSettings.Neagent.Bar.Enabled.value !== newSettings.Neagent.Bar.Enabled.value) {
						Helpers.injectNeagentBar(!newSettings.Neagent.Bar.Enabled.value)

						document.getElementsByClassName(notifierClass)[0].style.top = 
							(newSettings.Neagent.Bar.Enabled.value)? (newSettings.Neagent.Bar.HeightPx.value + 15) + 'px': '15px'
					}

					Settings = newSettings
					break
				case 'check-phone-number-ok':
					var normalizedPhone = message.phoneNumber
					var response = JSON.parse(message.response)
					var elements = document.getElementsByClassName(wrappedElementFormatString.format(normalizedPhone))

					for(var i = 0; i < elements.length; ++i) {
						elements[i].style = response.style
					}

					if (typeof response.error === 'undefined') {
						Notifier.notify({
							title: '<span class="' + wrappedElementPrefix + '" style="' + response.style + '">{0}</span>'
								.format(Helpers.phoneNumberToInternationalFormat(normalizedPhone)),
					    	content: response.description.text,
					    	autoClose: Settings.Popup.ClosePopupAfterMsec.value
					    })
					} else {
						Notifier.notify({
					    	content: response.error.description.text,
					    	autoClose: Settings.Popup.ClosePopupAfterMsec.value,
					    	error: true
					    })
					}

					break
				case 'log':
					alert(message.text)
					break
			}
		}

		function PhoneNumberHandler() {
			function makeWrapper(options) {
				var wrapper = document.createElement('span')

				if (options) {
					if (wrapper.style) {
						wrapper.style = options.style;
					}
					if (options.content) {
						wrapper.innerHTML = options.content
					}
					if (options.class) {
						wrapper.className = options.class
					}
				}
			    
			    return wrapper
			}

			this.pattern = patterns.humanPhoneNumber

			this.wrap = function(phone) {
				var normalizedPhone = Helpers.normalizePhoneNumber(phone)

				if (normalizedPhone) {
					var style = defaultStyle
					if (Cache && normalizedPhone in Cache) {
						var response = JSON.parse(Cache[normalizedPhone])
						if (response && typeof response.error === 'undefined') {
							style = response.style
						}
					}
					var wrapper = makeWrapper({
						content:phone,
						style:style,
						class:wrappedElementFormatString.format(normalizedPhone)
					})
					wrapper.onclick = function() {
						postMessage({
							type:'check-phone-number',
							phoneNumber:normalizedPhone
						})
					}
					return wrapper
				}
				return null
			}
		}

		function highlightElement(element, options) {
			if (Settings.General.IsPhoneNumberDetectionEnabled.value !== true) {
				return
			}
		    if (options.excludedTags.test(element.tagName)) {
		        return
		    }
		    if (options.excludedClassNames.test(element.className)) {
		    	return
		    }
		    
		    var nodes = element.childNodes
		    
		    for (var i = 0; i < nodes.length; ++i) {
		        var node = nodes.item(i)
		        
		        if (node instanceof window.Element) {
		            highlightElement(node, options)
		        }
		        else if (node instanceof window.Text) {
		            var subNodes = splitTextNode(node, options)
		            
		            if (subNodes.length > 0) {
		                for (var j = 0; j < subNodes.length; ++j) {
		                    element.insertBefore(subNodes[j], node)
		                }
		                
		                element.removeChild(node)
		                i += subNodes.length
		            }
		        }
		    }
		}

		function splitTextNode(textNode, options) {
		    var nodes = [textNode]
		    
		    for (var i = 0; i < options.handlers.length; ++i) {
		        var handler = options.handlers[i]
		        
		        for (var j = 0; j < nodes.length; ++j) {
		            var node = nodes[j]
		            
		            if (node instanceof window.Text) {
		                var splitNodes = splitTextNodeByHandler(node, handler)
		                
		                if (splitNodes.length > 0) {
		                    nodes.splice.apply(nodes, [j, 1].concat(splitNodes))
		                    j += splitNodes.length
		                }
		            }
		        }
		    }
		    
		    if ((nodes.length == 1) && (nodes[0] === textNode)) {
		        return []
		    }
		    else {
		        return nodes
		    }
		}

		function splitTextNodeByHandler(textNode, handler) {
		    var separator = '\0'
		    var matches = []
		    var nodes = []
		    
		    var textParts = textNode.nodeValue.replace(handler.pattern, function () {
		    	var wpapped = handler.wrap.apply(handler, arguments)
		        wpapped && matches.push(wpapped)
		        return separator
		    }).split(separator)
		    
		    if (matches.length == 0) {
		        return nodes
		    }
		    
		    while (textParts.length > 0) {
		        var text = textParts.shift()
		        var match = matches.shift()
		        
		        if (text.length > 0) {
		            nodes.push(document.createTextNode(text))
		        }
		        
		        if (match instanceof Array) {
		            nodes.push.apply(nodes, match)
		        }
		        else if (match != undefined) {
		            nodes.push(match)
		        }
		    }
		    
		    return nodes
		}

		function runDetection() {
			var options = {
		        excludedTags: /^(?:a|applet|area|button|embed|frame|frameset|head|iframe|img|input|link|map|meta|object|option|param|script|select|statusbar|style|textarea|title)$/i,
		        excludedClassNames: new RegExp('^(?:' + wrappedElementPrefix + ')', 'i'),
		        handlers: [new PhoneNumberHandler()]
		    }
		    
		    highlightElement(document.documentElement, options)

		    if (Settings.Rules.ElentHandling.Disallow.DOMNodeInserted.value.contains(document.domain)) {
		    	// disable DOMNodeInserted event handling for specified domains
		    } else {
			    document.addEventListener('DOMNodeInserted', function (event) {
			        highlightElement(event.target, options)
			    }, false)
			}
		}
	})
})()